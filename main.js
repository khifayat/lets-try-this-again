const api = {
  key: "5bc5e89102356d4703326f3fe0a4499f",
  baseurl: "api.openweathermap.org/data/2.5/"
}

const searchbox = document.querySelector('.search-box');
searchbox.addEventListener('keypress', setQuery);

function setQuery(evt){
if(evt.keycode == 13){
   getResults(searchbox.value);
   console.log(searchbox.value);
}
}

function getResults (query){
fetch('${api.base}weather?q=(query)&units=imperial&APPID= $(api.key)')
.then(weather =>{
return weather.json();
}).then(displayResults);
}

function displayResults(weather){
  console.log(weather);
}
